// console.log('hej');
// import file
const readline = require('readline');
const os = require('os');
const fs = require ('fs');

const http = require('http');
const server = http.createServer((req, res) => {
    res.write('Hello node-JS World!');
    res.end();
});

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const message = `
Choose an option:
1. Read my package.json
2. Get my OS info
3. Start a HTTP server
Type a number: `

rl.question(message, (answer)=>{
    if (answer == 1) {
        fs.readFile(__dirname + '/package.json','utf-8', (err, content)=>{
            console.log(content);
        });
    } else if (answer == 2){
        console.log("SYSTEM MEMORY: ",(os.totalmem() / 1024 / 1024 / 1024).toFixed(2) + ' GB');
        console.log("FREE MEMORY: " ,(os.freemem() / 1024 / 1024 / 1024).toFixed(2) + ' GB');
        const cpus = os.cpus();
        console.log("CPU CORES: " + cpus.length );
        console.log("ARCH: " + os.arch());
        console.log("PLATFORM: " + os.platform());
        console.log("RELEASE: " + os.release());
        const user = os.userInfo();
        console.log("USER: " + user.username);
    } else if (answer == 3) {
        server.listen(3000);
    } else {
        console.log('Not a valid option');
    }
    console.log(answer);
    rl.close();
});

//rl.question(message, (answer)=>{
//
//    if (answer == 2) {
//        console.log('Nice work! That is correct')
//    }
//    console.log(answer);
//    rl.close();
//});