const fs = require('fs');
const eol = require('os').EOL;

// console.log(JSON.stringify(eol));

// console.log(__dirname);
// console.log(__filename);

// setTimeout(function(){
//    console.log('hej');   
//}, 2000);

// setTimeout(function(){
//    console.log('hej igen');   
//}, 3000);

fs.readFile(__dirname + '/mytext.csv', 'utf-8', (err, contents) => {

    //    console.log('error: ', err);
    //    console.log('File contents: ', contents);

    const rows = contents.split(eol);
    console.log(rows)

    const qoutes = rows.map(row => {

        const cols = row.split(',')

        return {
            name: cols[0],
            quoute1: cols[1],
            quoute2: cols[2]
        };

    });
    console.log(qoutes);


});

